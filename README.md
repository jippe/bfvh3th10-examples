# Web Based Information Systems / Theme 10 Example project(s) #

## Prerequisites ##

The included example(s) require the following - external - software package to be present:

* openssl - required for running the benchmarking example, installed on most Unix systems and macOS by default. See [https://www.openssl.org](https://www.openssl.org)

## Installation ##

* Clone the project, 
* edit the `web.xml` file located in `./src/main/webapp/WEB-INF/` to point to a temporary storage location,
* run the gradle `war` build task: `./gradlew war` and
* place the resulting war file (`./build/libs/<project>-<version>.war`) in a container engine (i.e. Tomcat)

Or open the project in IntelliJ, add a run configuration and launch from there.

## Usage ##

Visit the following page(s) for demonstration(s):

* OpenSSL Benchmark Suite: [`http://localhost:8080/benchmark`](http://localhost:8080/benchmark) (depends on server name and port number)
* Overview of the most recent earthquakes in the Netherlands (and surrounding area) as provided by the KNMI: [`http://localhost:8080/mapping.html`].
    * The Servlet involved only provides the data (earthquake events as JSON) as no template rendering takes place.
    * Map visualization is possible through the use of the [ArcGIS Javascript library](https://developers.arcgis.com/javascript/)