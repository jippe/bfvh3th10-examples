package nl.bioinf.marcelk.web2018.model;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;

public class Earthquake {
    private long date;
    private String location;
    private String[] latLon;
    private float depth;
    private float magnitude;

    public Earthquake(String date, String location, String latitude,
                      String longitude, String depth, String magnitude) {
        setDate(date);
        this.location = location;
        setCoordinates(latitude, longitude);
        setDepth(depth);
        setMagnitude(magnitude);
    }

    public long getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = LocalDateTime.parse(date).toEpochSecond(ZoneOffset.UTC);
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String[] getLatLon() {
        return latLon;
    }

    public void setCoordinates(String latitude, String longitude) {
        String[] coordinates = {latitude, longitude};
        this.latLon = coordinates;
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = Float.parseFloat(depth);
    }

    public float getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(String magnitude) {
        this.magnitude = Float.parseFloat(magnitude);
    }

    @Override
    public String toString() {
        return "Earthquake{" +
                "date=" + date +
                ", location='" + location + '\'' +
                ", latLon=" + Arrays.toString(latLon) +
                ", depth=" + depth +
                ", magnitude=" + magnitude +
                '}';
    }
}
