package nl.bioinf.marcelk.web2018.servlets;

import com.google.gson.Gson;
import nl.bioinf.marcelk.web2018.service.SSLBenchmarkResultsParser;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * This servlet only serves data that is used to populate results table(s) with benchmark statistics.
 * Depending on the state of the benchmark (either 'running' or 'finished'), different data is served.
 */
@WebServlet(urlPatterns = "/results")
public class ResultsDataServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Either 'running' or 'finished', provides data to two different AJAX calls
        String type = request.getParameter("status");

        // Session information
        HttpSession session = request.getSession(false);
        String sessionid = session.getId();

        // Output folder from 'web.xml' init parameter
        String outputFolder = getServletContext().getInitParameter("output.storage");

        // Parse results
        SSLBenchmarkResultsParser parser = new SSLBenchmarkResultsParser();

        // Parse output based on requested status type and create a JSON object
        String json = "";
        if (type.equals("running")) {
            List<List<String>> output = parser.getParsedRunningResults(outputFolder, sessionid);
            json = new Gson().toJson(output);
        } else if (type.equals("finished")) {
            List<List<String>> output = parser.getParsedFinishedResults(outputFolder, sessionid);
            if (output.size() == 0)
                json = new Gson().toJson(null);
            else
                json = new Gson().toJson(output);
        }

        // Return results as JSON
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
