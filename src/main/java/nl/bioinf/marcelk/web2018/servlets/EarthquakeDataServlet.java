package nl.bioinf.marcelk.web2018.servlets;

import nl.bioinf.marcelk.web2018.service.EarthquakeDataFetcher;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EarthquakeDataServlet", urlPatterns = "/mapdata")
public class EarthquakeDataServlet extends HttpServlet {

    /**
     * Retrieves data regarding recent earthquakes from the KNMI. Data is returned as JSON.
     * @param request
     * @param response
     * @throws IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        EarthquakeDataFetcher fetcher = new EarthquakeDataFetcher();
        String limit = request.getParameter("limit");
        String json = fetcher.getEarthquakes(Integer.valueOf(limit));

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
