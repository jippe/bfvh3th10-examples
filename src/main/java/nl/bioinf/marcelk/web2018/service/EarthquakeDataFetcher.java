package nl.bioinf.marcelk.web2018.service;

import com.google.gson.Gson;
import nl.bioinf.marcelk.web2018.model.Earthquake;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;

public class EarthquakeDataFetcher {

    /**
     * Fetches most recent earthquakes registered at KNMI.
     * @param limit Number of most recent events to retrieve
     * @return JSON Formatted list of earthquakes
     */
    public String getEarthquakes(int limit) {
        HttpClient client = HttpClient.newHttpClient();
        try {

            // Properly format the request URI, including GET parameters
            URI knmiURI = new URIBuilder("http://rdsa.knmi.nl/fdsnws/event/1/query")
                    .addParameter("format", "text")
                    .addParameter("limit", Integer.toString(limit))
                    .build();

            // Fetch the data and process the response body as String
            HttpResponse<String> knmiResponse = client.send(
                    HttpRequest
                            .newBuilder(knmiURI)
                            .timeout(Duration.of(10, SECONDS))
                            .GET()
                            .build(), HttpResponse.BodyHandlers.ofString());


            // If retrieval was successful, convert response string to JSON and return
            if (knmiResponse.statusCode() == 200) {
                String body = knmiResponse.body();
                String json = responseToJSON(body);
                return json;
            }
        } catch (URISyntaxException | InterruptedException | IOException e) {
            e.printStackTrace();
        }

        return "failed";
    }

    /**
     * Given a String containing the full KNMI response (multiple records, see example in the main method),
     * converts this to a list of Earthquake objects and subsequently converting and returning this as a JSON String.
     * @param response
     * @return
     */
    public String responseToJSON(String response) {
        List<Earthquake> data = new ArrayList<Earthquake>();
        String earthquakes[] = response.split("\\r?\\n");
        for (String earthquake: earthquakes) {
            if (earthquake.startsWith("#"))
                continue;
            String fields[] = earthquake.split("\\|");
            Earthquake eq = new Earthquake(
                    fields[1],  // Date
                    fields[12], // Location
                    fields[2],  // Lat
                    fields[3],  // Lon
                    fields[4],  // Depth
                    fields[10]);// Magnitude
            data.add(eq);
        }
        return new Gson().toJson(data);
    }

    public static void main(String[] args) {
        /*
        KNMI Response example (single String) for the most recent 5 earthquakes:
        ------------------------------------------------------------------------
            #EventID|Time|Latitude|Longitude|Depth/km|Author|Catalog|Contributor|ContributorID|MagType|Magnitude|MagAuthor|EventLocationName
            knmi2019wbmj|2019-11-10T21:10:06.2|53.142000|6.805000|3.0|||KNMI|knmi2019wbmj|MLn|0.8439769202||Sappemeer
            knmi2019vzwb|2019-11-09T23:47:21.699999|53.247000|6.998000|3.0|||KNMI|knmi2019vzwb|MLn|0.5328950989||Nieuwolda
            knmi2019vvun|2019-11-07T18:28:39.1|50.867000|5.735000|15.0|||KNMI|knmi2019vvun|MLn|0.9928871264||Maastricht
            knmi2019vocw|2019-11-03T13:35:43.5|50.723000|7.195000|7.0|||KNMI|knmi2019vocw|MLs|1.242464941||Königswinter (Duitsland)
            knmi2019vdlv|2019-10-28T17:38:33.0000|53.315000|6.748000|3.0|||KNMI|knmi2019vdlv|MLn|1.240826591||Garrelsweer
        ------------------------------------------------------------------------
         */

        EarthquakeDataFetcher fetch = new EarthquakeDataFetcher();
        String json = fetch.getEarthquakes(5);
        System.out.println(json);

        /*
        Example conversion to JSON of a single earthquake:
        --------------------------------------------------
        [
            {
                "date": 1573420206,
                "location": "Sappemeer",
                "latLon": [
                    "53.142000",
                    "6.805000"
                ],
                "depth": 3,
                "magnitude": 0.8439769
            }, ...
        ]
         */
    }
}
